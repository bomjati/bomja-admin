<?php

namespace Deployer;

require 'recipe/rsync.php';
require 'recipe/laravel.php';

inventory('deployment/hosts.yml');

set('application', 'bomja-admin');

set('repository', 'git@bitbucket.org:bomjati/bomja-admin.git');
set('keep_releases', 2);
set('rsync_src', __DIR__);
set('rsync_dest', '{{deploy_path}}');

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:symlink',
    // 'artisan:config:cache',
    // 'artisan:route:cache',
    // 'artisan:view:cache',
    // 'artisan:storage:link',
    // 'artisan:optimize',
    // 'deploy:info',
    // 'deploy:prepare',
    // 'deploy:lock',
    // 'deploy:release',
    // 'rsync',
    // 'deploy:update_code',
    // 'deploy:shared',
    // 'deploy:vendors',
    // 'deploy:writable',
    // 'artisan:storage:link',
    // 'artisan:view:clear',
    // 'artisan:cache:clear',
    // 'artisan:optimize',
    // 'deploy:symlink',
    // 'deploy:unlock',
    // 'cleanup',

])->desc('Deploy project ');

after('deploy', 'success');
