FROM php:7.3


## install SSH and Rsync
RUN apt-get update && apt-get install -y unzip rsync ssh

## install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

## install Deployer with recipes

RUN composer global require deployer/deployer
RUN composer global require deployer/recipes --dev

## add Composer vendor into PATH
ENV PATH /root.composer/vendor/bin:$PATH
