<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_configurations', function (Blueprint $table) {
            $table->id();
            $table->string('primaryColor')->nullable();
            $table->string('secondaryColor')->nullable();
            $table->string('typeShowcases')->nullable();
            $table->string('offerPhrase')->nullable();
            $table->string('imgName')->nullable();
            $table->string('pathS3')->nullable();
            $table->time('timeToOpen')->nullable();
            $table->time('closeTime')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_configurations');
    }
}
