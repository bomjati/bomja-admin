<?php

use App\Models\Product\ProductCategory;
use Illuminate\Database\Seeder;


class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Café da manhã'],
            ['id' => 2, 'name' => 'Padaria'],
            ['id' => 3, 'name' => 'Mercearia em geral'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 5, 'name' => 'Bebidas'],
            ['id' => 6, 'name' => 'Carnes'],
            ['id' => 7, 'name' => 'frios'],
            ['id' => 8, 'name' => 'Produtos de limpeza'],
            ['id' => 9, 'name' => 'Utilidades'],
            ['id' => 10, 'name' => 'Higiene pessoal'],
            ['id' => 11, 'name' => 'Frutas'],
            ['id' => 12, 'name' => 'legumes'],
            ['id' => 13, 'name' => 'Pet'],

        ];

        foreach ($items as $item) {

            ProductCategory::create($item);
        }
    }
}
