<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminUserSeeder extends Seeder
{
    public function run(){
        // cria usuario admin
        $user = User::create([
        'name' => 'AokiMK',
        'email' => 'maycoaoki@gmail.com',
        'password' => bcrypt('123456')
    ]);
        $role = Role::find(1); //cria role admin
        $permissions = Permission::pluck('id','id')->all();// atribui as regras de permissão ao id do admin
        $role->syncPermissions($permissions);//sincroniza permissões ao admin
        $user->assignRole([$role->id]);//associa o id do admin as regras
    }
}
