<?php

use App\Models\Content\CategoryContent;
use Illuminate\Database\Seeder;

class ContentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Vinheta'],
            ['id' => 2, 'name' => 'Comerciais'],
            ['id' => 3, 'name' => 'Institucionais'],
            ['id' => 4, 'name' => 'Programetes'],
            ['id' => 5, 'name' => 'Jingle'],
            ['id' => 6, 'name' => 'Sazonais'],
            ['id' => 7, 'name' => 'Outros'],


        ];

        foreach ($items as $item) {

           CategoryContent::create($item);
        }
    }
}
