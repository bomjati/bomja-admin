<?php

use App\Models\Media\Genre;
use Illuminate\Database\Seeder;


class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Axé'],
            ['id' => 2, 'name' => 'Blues'],
            ['id' => 3, 'name' => 'Country'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],
            ['id' => 4, 'name' => 'Enlatados'],

        ];
    }
}
