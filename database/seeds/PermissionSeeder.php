<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    public function run(){
        $permissions = [
            //permissões de usuario
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            //permissões para criar regra
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            //permissões para ativar e desativar usuario
            'off-list',
            'off-restore'
        ];
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
