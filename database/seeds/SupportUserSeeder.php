<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SupportUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([

        	'name' => 'Shoko',
        	'email' => 'shoko@bunny.com',
        	'password' => bcrypt('123456')

        ]);

        $role = Role::find(2);
        $user->assignRole([$role->id]);
    }
}
