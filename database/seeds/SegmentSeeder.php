<?php

use App\Models\Company\Segment;
use Illuminate\Database\Seeder;

class SegmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Bebidas'],
            ['name' => 'Calçado'],
            ['name' => 'Couros'],
            ['name' => 'Gráfica'],
            ['name' => 'Mobiliário'],
            ['name' => 'Metalurgia'],
            ['name' => 'Mecânica'],
            ['name' => 'Vestuário'],
            ['name' => 'Armarinho'],
            ['name' => 'Acessórios'],
            ['name' => 'Combustíveis'],
            ['name' => 'Ferragens'],
            ['name' => 'Roupas'],
            ['name' => 'Supermercado'],
            ['name' => 'Tecidos'],
            ['name' => 'Veículos'],
            ['name' => 'Alimentação'],
            ['name' => 'Cinema'],
            ['name' => 'Educação'],
            ['name' => 'Lazer'],
            ['name' => 'Saúde'],
            ['name' => 'Transporte'],
            ['name' => 'Turismo'],

        ];

        foreach ($items as $item) {
            Segment::create($item);
        }
    }
}
