<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    //ordem de execução das seeds
    public function run()
    {
        $this->call([

            RoleSeeder::class,
            PermissionSeeder::class,
            AdminUserSeeder::class,
            SupportUserSeeder::class,
            ModeratorUserSeeder::class,
            UserSeeder::class,
            ProductCategorySeeder::class,
            HeapSeeder::class,
            ContentCategorySeeder::class,
            SegmentSeeder::class

        ]);
    }
}
