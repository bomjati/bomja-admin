<?php


use App\Models\Media\Heap;
use Illuminate\Database\Seeder;

class HeapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Acervo-01'],
            ['id' => 2, 'name' => 'Acervo-02'],
            ['id' => 3, 'name' => 'Acervo-Amurco-01'],
            ['id' => 4, 'name' => 'Acervo-Amurco-02'],

        ];

        foreach ($items as $item) {

            Heap::create($item);
        }
    }
}
