<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ModeratorUserSeeder extends Seeder
{
    public function run(){
        //cria usuario
        $user = User::create([
        	'name' => 'Sakura',
        	'email' => 'sakura@konoha.com',
        	'password' => bcrypt('123456')
        ]);
        $role = Role::find(3);// cria role
        $user->assignRole([$role->id]);//associa o role ao id do usuario criado
    }
}
