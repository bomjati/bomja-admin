@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Product</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product.index') }}">Back</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <br>
    <div class="row justify-content-md-center">
        <div class="card" style="width: 40rem;">
            <div class="card-header">
                Register
            </div>
            <form action="{{ route('product.update', $product->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="col">
                    <img class="rounded mx-auto d-block" style="max-width: 200px;" src="{{ $product->productImg->path_s3 }}"
                        alt="">
                </div>


                <div class="col"> <label for="">Nome</label>
                    <input class="form-control" type="text" name="name" value="{{ $product->name }}">
                </div>
                <div class="col">
                    <label for="">Proço</label>
                    <input class="form-control" type="text" name="price" value="{{ $product->price }}">
                </div>
                <div class="col">
                    <label for="">Preço Promoção</label>
                    <input class="form-control" type="text" name="promotion_price" value="{{ $product->promotion_price }}">
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="detail">Detalhes</label>
                        <textarea class="form-control" name="detail" id="detail" rows="3">{{ $product->detail }}</textarea>
                    </div>
                </div>
                <div class="col">
                    <br>
                    <select name="category" class="custom-select custom-select-sm">
                        <option value="{{ $product->productCategory->id }}" selected>{{ $product->productCategory->name }}
                        </option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div><br>
                <div class="row justify-content-md-center">

                    <div class="col-4">
                        <label for="birthday">Data Inicial</label>
                        <input type="date" class="form-control" name="date_start" id="date_start"
                            value="{{ $product->date_start }}">
                    </div>
                    <div class="col-4">
                        <label for="birthday">Tempo Inicial</label>
                        <input type="time" class="form-control" name="time_start" id="time_start"
                            value="{{ $product->time_start }}">
                    </div>

                </div>
                <div class="row justify-content-md-center">
                    <div class="col-4">
                        <label for="birthday">Data Final</label>
                        <input type="date" class="form-control" name="date_end" id="date_end"
                            value="{{ $product->date_end }}">
                    </div>
                    <div class="col-4">
                        <label for="birthday">Tempo Final</label>
                        <input type="time" class="form-control" name="time_end" id="time_end"
                            value="{{ $product->time_end }}">
                    </div>
                </div>
                <div class="col">
                    <br>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="img" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="col">
                    <br>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>

@endsection
