@extends('layouts.master')

@section('content')
    <br>
    <div class="row justify-content-md-center">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Comment Edit</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product.show', $comment[0]->product_id) }}">Back</a>
            </div>
        </div>
    </div>
    <div class="row justify-content-md-center">
        <div class="col-6">
            <form action="{{ route('comment.update', $comment[0]->id) }}" method="POST">
                @csrf
                @method('PUT')
                <strong>Alterar Post</strong>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Escreva aqui" aria-label="Recipient's username"
                        aria-describedby="button-addon2" name="comment" value="{{ $comment[0]->comment }}">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Button</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
