@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Product</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product.index') }}">Back</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row justify-content-md-center">
        <div class="col-10">
            <br>
            <div class="card">
                <div class="row justify-content-md-center p-3">
                    <img src="{{ $product[0]->productImg->path_s3 }}" alt="" width="100" height="200">
                </div>
                <hr>
                <div class="row justify-content-md-center">
                    <form action="{{ route('product.like', $product[0]->id) }}" method="POST">
                        @csrf
                        <div class="row justify-content-md-center">
                            <div class="col">
                                <button type="submit" name="like" value="0"
                                    class="btn btn-ligth material-icons border">thumb_down</button>
                            </div>
                            <div class="col">
                                <button type="submit" name="like" value="1"
                                    class="btn btn-ligth material-icons border">thumb_up</button>
                            </div>
                        </div>
                    </form>
                    <div class="col-1">
                        <button type="button" class="btn btn-primary">
                            <span class="badge badge-light">{{ $like }}</span>
                        </button>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <form action="{{ route('comment.create', $product[0]->id) }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <input class="form-control" type="text" name="comment">
                            </div>
                            <div class="col-2">
                                <button type="submit" class="btn btn-success">Post</button>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @foreach ($comments as $comment)
                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col-10">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">{{ $comment->user->name }}</span>
                                    </div>
                                    <input type="text" class="form-control" value="{{ $comment->comment }}" name="comment"
                                        aria-label="Username" aria-describedby="basic-addon1" disabled>
                                </div>
                            </div>
                            <form action="{{ route('comment.destroy', $comment->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="row">
                                    <div class="col">
                                        <a href="{{ route('comment.edit', $comment->id) }}"
                                            class="btn btn-warning material-icons">edit</a>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-danger material-icons">delete</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
