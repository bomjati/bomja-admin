@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Product</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('product.create') }}">New Product</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <div class="row row-cols-1 row-cols-md-3">

        @foreach ($products as $product)
            <div class="card">
                <div class="col">
                    <img src="{{ $product->productImg->path_s3 }}" class="card-img-top" alt="..." width="20" height="100">
                    <div class="card-body">
                        <p>Produto: {{ $product->name }}</p>
                        <p>Categoria: {{ $product->productCategory->name }}</p>
                        <form action="{{ route('product.destroy', $product->id) }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <a href="{{ route('product.edit', $product->id) }}" class="btn btn-info"><i
                                    class="fas fa-edit"></i></a>

                            <a href="{{ route('product.show', $product->id) }}" class="btn btn-success"><i
                                    class="fas fa-eye"></i></a>

                            <button type="submit" class="btn btn-danger"><i class="fas fa-times-circle"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col">
                    <form action="{{ route('product.view', $product->id) }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-secondary material-icons">add_circle_outline</button>

                    </form>
                </div>
            </div>
        @endforeach
    @endsection
