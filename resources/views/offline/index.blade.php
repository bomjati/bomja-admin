@extends('layouts.master')


@section('content')

<div class="container">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12 margin-tb">

                <div class="pull-left">

                    <h2>Users Offline</h2>

                </div>

            </div>

        </div>

        <br>
        @if ($message = Session::get('success'))

        <div class="alert alert-success">

          <p>{{ $message }}</p>

        </div>

        @endif


        <table class="table table-bordered">

         <tr>

           <th>No</th>

           <th>Name</th>

           <th>Email</th>

           <th>Roles</th>

           <th width="280px">Action</th>

         </tr>

         @foreach ($userTrashed as $key => $user)

          <tr>

            <td>{{ $user->id }}</td>

            <td>{{ $user->name }}</td>

            <td>{{ $user->email }}</td>

            <td>

              @if(!empty($user->getRoleNames()))

                @foreach($user->getRoleNames() as $v)

                   <label class="badge badge-success">{{ $v }}</label>

                @endforeach

              @endif

            </td>

            <td>

               <a class="btn btn-info" href="{{ route('offline.restore',$user->id) }}">Restore</a>

                {!! Form::close() !!}

            </td>

          </tr>

         @endforeach

        </table>
    </div>
</div>
@endsection
