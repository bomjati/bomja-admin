@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>New Content</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('content.index') }}">Back</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <br>
    <div class="row justify-content-md-center">
        <div class="card" style="width: 40rem;">
            <div class="card-header">
                Featured
            </div>
            <form action="{{ route('content.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col">
                    <strong>Segment</strong>
                    <select name="segment" class="custom-select">
                        <option value="">Open this select menu</option>
                        @foreach ($segments as $segment)
                            <option value="{{ $segment->id }}">{{ $segment->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <strong>Category</strong>
                    <select name="category" class="custom-select">
                        <option value="">Open this select menu</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <strong>SubCategory</strong>
                    <input class="form-control" type="text" name="subCategory">
                </div>
                <div class="col">
                    <strong>Users</strong>
                    <select class="custom-select" name="user[]" multiple>
                        <option value="" selected>Open this select menu</option>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <strong>File</strong>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="music[]" id="customFile" multiple>
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <div class="col">
                    <strong>Date Start</strong>
                    <input type="date" class="form-control" name="dateStart" id="dateStart">
                </div>
                <div class="col">
                    <strong>Time Start</strong>
                    <input type="time" class="form-control" name="timeStart" id="timeStart">
                </div>
                <div class="col">
                    <strong>Date Start</strong>
                    <input type="date" class="form-control" name="dateEnd" id="dateEnd">
                </div>
                <div class="col">
                    <strong>Time End</strong>
                    <input type="time" class="form-control" name="timeEnd" id="timeEnd">
                </div>
                <div class="col">
                    <br>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
