@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Music</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-success" href="{{ route('heap.create') }}">New Genre</a>
                @endcan
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <div class="row">
        @foreach ($genres as $genre)
            <div class="col-4">
                <br>
                <div class="card" style="width: 18rem;">
                    <img src="{{ $genre->cover->path_s3 }}" class="card-img-top" alt="..." width="200" height="200">
                    <div class="card-body">
                        <h5 class="card-title">Gênero: {{ $genre->name }}</h5>
                        <p class="card-text">Acervo: {{ $genre->heap->name }}</p>
                        <a href="{{ route('heap.show', $genre->id) }}" class="btn btn-primary">Musicas</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
