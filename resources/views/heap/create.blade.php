@extends('layouts.master')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <br>
    <div class="row justify-content-md-center">
        <div class="card" style="width: 30rem;">
            <div class="card-header">
                Novo Gênero
            </div>
            <form action="{{ route('heap.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col">
                    <div class="form-row align-items-center">
                        <div class="col">
                            <label for="inlineFormCustomSelect">Arcevos</label>
                            <select class="custom-select mr-sm-2" name="acervo" id="acervo">
                                <option value="" selected>Choose...</option>
                                @foreach ($heaps as $heap)
                                    <option value="{{$heap->id}}" >{{$heap->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <label for="">Gênero</label>
                    <input class="form-control" type="text" name="genre">
                </div>
                <div class="col p-3">
                    <div class="custom-file">
                        <input type="file" name="img" class="custom-file-input" id="img" multiple />
                        <label class="custom-file-label" for="customFile">Imagem da capa do genero</label>
                    </div>
                </div>
                <div class="col p-3">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection
