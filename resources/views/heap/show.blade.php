@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Music by genre</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-primary" href="{{ route('heap.index') }}">Back</a>
                @endcan
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Artista|Banda</th>
                        <th scope="col">Música</th>
                        <th scope="col">Tempo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($genre as $item)
                        <tr>
                            <th scope="row">{{ $item->id }}</th>
                            <td>{{ $item->artist }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->totalTime }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
