@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="container-fluid">
            <br>
            <div class="row">
                <div class="col-lg-6 margin-tb">
                    <div class="pull-left">
                        <h2>Register Company</h2>
                    </div>
                </div>
                <div class="col-lg-6 margin-tb">
                    <div class="pull-right">
                        @can('user-create')
                            <a class="btn btn-primary" href="{{ route('company.index') }}">Back</a>
                        @endcan
                    </div>
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <br>
            {{-- main --}}
            <form action=" {{ route('company.store') }} " method="POST">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label for="registration_number">CNPJ</label>
                        <input id="registration_number" class="form-control" type="text" name="registration_number" required
                            maxlength="14" size="10">
                    </div>
                </div>
                <div class="row">
                    <div class="col-7">
                        <label for="business_name">Nome Empresarial</label>
                        <input id="business_name" class="form-control" type="text" name="business_name">
                    </div>
                </div>
                <div class="row">
                    <div class="col-7">
                        <label for="fantasy_name">Nome Fantasia</label>
                        <input id="fantasy_name" class="form-control" type="text" name="fantasy_name">
                    </div>
                </div>
                <strong>Endereço</strong>
                <div class="row">
                    <div class="col-3">
                        <label for="cep">CEP</label>
                        <input id="cep" class="form-control" type="text" name="cep" required maxlength="8" size="10">
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label for="public_place">Logradouro</label>
                        <input id="public_place" class="form-control" type="text" name="public_place">
                    </div>
                    <div class="col-2">
                        <label for="number">Nº</label>
                        <input id="number" class="form-control" type="text" name="number" required maxlength="6" size="10">
                    </div>
                    <div class="col-1">
                        <label for="uf">UF</label>
                        <input id="uf" class="form-control" type="text" name="uf" required maxlength="2" size="10">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label for="complement">Complemento</label>
                        <input id="complement" class="form-control" type="text" name="complement">
                    </div>
                    <div class="col-2">
                        <label for="district">Bairro</label>
                        <input id="district" class="form-control" type="text" name="district">
                    </div>
                    <div class="col-2">
                        <label for="county">Cidade</label>
                        <input id="county" class="form-control" type="text" name="county">
                    </div>
                </div>
                <strong>Contatos</strong>
                <div class="row">
                    <div class="col-7">
                        <label for="email">Email</label>
                        <input id="email" class="form-control" type="email" name="email">
                    </div>
                </div>
                <div class="row">
                    <div class="col-7">
                        <label for="phone1">Tel 1:.</label>
                        <input id="phone1" class="form-control" type="text" name="phone1" required maxlength="11" size="10">
                    </div>
                </div>
                <div class="row">
                    <div class="col-7">
                        <label for="phone2">Tel 2:.</label>
                        <input id="phone2" class="form-control" type="text" name="phone2" required maxlength="11" size="10">
                    </div>
                </div>
                <div class="row">
                    <div class="col-7">
                        <strong>Segmentos</strong>
                        <select name="segment" class="custom-select">
                            <option value="">Open this select menu</option>
                            @foreach ($segments as $segment)
                                <option value="{{ $segment->id }}">{{ $segment->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-7">
                        <br>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="term" value="true" id="invalidCheck" required>
                                <label class="form-check-label" for="invalidCheck">
                                    Agree to terms and conditions
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-7">
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </div>
            </form>
            {{-- finish --}}
        </div>
    </div>
@endsection
