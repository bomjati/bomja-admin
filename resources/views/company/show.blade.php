@extends('layouts.master')

@section('content')
<div class="container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 margin-tb">
                <div class="pull-left">
                    <h2>Show Company</h2>
                </div>
            </div>
            <div class="col-lg-6 margin-tb">
                <div class="pull-right">
                    @can('user-create')
                    <a class="btn btn-primary" href="{{ route('company.index') }}">Back</a>
                    @endcan
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <br>
        {{--  main  --}}

                <div class="card">
                    <div class="card-header">
                      {{$company->fantasy_name}}
                    </div>
                    <div class="card-body">
                        <div class="row border">
                            <div class="col-3 border">
                                <strong>CNPJ:</strong>
                                {{$company->registration_number}}
                            </div>
                        </div>
                        <div class="row border">
                            <div class="col border">
                                <strong>Nome Empresarial:</strong>
                                {{$company->business_name}}
                            </div>
                        </div>
                        <div class="row border">
                            <div class="col">
                                <strong>CEP:</strong>
                                {{$company->address->cep}}
                            </div>
                        </div>
                        <div class="row border">
                            <div class="col border">
                                <strong>Logradouro:</strong>
                                {{$company->address->public_place}}
                            </div>
                            <div class="col border">
                                <strong>Numero:</strong>
                                {{$company->address->number}}
                            </div>
                            <div class="col border">
                                <strong>Complemento:</strong>
                                {{$company->address->complement}}
                            </div>
                        </div>
                        <div class="row border">
                            <div class="col border">
                                <strong>Bairro:</strong>
                                {{$company->address->district}}
                            </div>
                            <div class="col border">
                                <strong>Municipio:</strong>
                                {{$company->address->county}}
                            </div>
                            <div class="col border">
                                <strong>UF:</strong>
                                {{$company->address->uf}}
                            </div>
                        </div>
                        <div class="row border">
                            <div class="col border">
                                <strong>Email:</strong>
                                {{$company->contact->email}}
                            </div>
                            <div class="col border">
                                <strong>Telefone 1:</strong>
                                {{$company->contact->phone1}}
                            </div>
                            <div class="col border">
                                <strong>Telefone 2:</strong>
                                {{$company->contact->phone2}}
                            </div>
                        </div>


                    </div>
                    <div class="card-footer text-muted">
                    </div>
                  </div>



        {{--  finish main  --}}

    </div>
</div>
@endsection
