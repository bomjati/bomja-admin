@extends('layouts.master')

@section('content')
<div class="container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 margin-tb">
                <div class="pull-left">
                    <h2>Edit Company</h2>
                </div>
            </div>
            <div class="col-lg-6 margin-tb">
                <div class="pull-right">
                    @can('user-create')
                    <a class="btn btn-primary" href="{{ route('company.index') }}">Back</a>
                    @endcan
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <br>
        <form action=" {{ route('company.update', $company->id) }} " method="POST">
            @csrf
            @method('PUT')
        <div class="row">
            <div class="col-3">
                <label for="registration_number">CNPJ</label>
                <input id="registration_number" class="form-control" type="text" name="registration_number" value=" {{$company->registration_number}} " required maxlength="14" size="10">
            </div>
        </div>
        <div class="row">
            <div class="col-7">
                <label for="business_name">Nome Empresarial</label>
                <input id="business_name" value=" {{$company->business_name}} " class="form-control" type="text" name="business_name">
            </div>
        </div>
        <div class="row">
            <div class="col-7">
                <label for="fantasy_name">Nome Fatasia</label>
                <input id="fantasy_name" class="form-control" type="text" value="{{$company->fantasy_name}}" name="fantasy_name">
            </div>
        </div>
        <strong>Endereço</strong>
        <div class="row">
            <div class="col-3">
                    <label for="cep">CEP</label>
                    <input id="cep" class="form-control" type="text" value="{{$company->address->cep}}" name="cep">
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <label for="public_place">Logradouro</label>
                <input id="public_place" class="form-control" type="text" value="{{$company->address->public_place}}" name="public_place">
            </div>
            <div class="col-2">
                <label for="number">Nº</label>
                <input id="number" class="form-control" type="text" value="{{$company->address->number}}" name="number" required maxlength="6" size="10">
            </div>
            <div class="col-1">
                <label for="uf">UF</label>
                <input id="uf" class="form-control" type="text" value="{{$company->address->uf}}" name="uf" required maxlength="2" size="10">
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <label for="complement">Complemento</label>
                <input id="complement" class="form-control" type="text" value="{{$company->address->complement}}" name="complement">
            </div>
            <div class="col-2">
                <label for="district">Bairro</label>
                <input id="district" class="form-control" type="text" value="{{$company->address->district}}" name="district">
            </div>
            <div class="col-2">
                <label for="county">Cidade</label>
                <input id="county" class="form-control" type="text" value="{{$company->address->county}}" name="county">
            </div>
        </div>
        <strong>Contatos</strong>
            <div class="row">
                <div class="col-7">
                    <label for="email">Email</label>
                    <input id="email" class="form-control" type="email" value="{{$company->contact->email}}" name="email">
                </div>
            </div>
            <div class="row">
                <div class="col-7">
                    <label for="phone1">Tel 1:.</label>
                    <input id="phone1" class="form-control" type="text" value="{{$company->contact->phone1}}" name="phone1" required maxlength="11" size="10">
                </div>
            </div>
        <div class="row">
            <div class="col-7">
                <label for="phone2">Tel 2:.</label>
                <input id="phone2" class="form-control" type="text" value="{{$company->contact->phone2}}" name="phone2" required maxlength="11" size="10">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-7">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>
 </div>
</div>
@endsection
