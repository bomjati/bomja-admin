@extends('layouts.master')

@section('content')
<div class="container">
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-6 margin-tb">
                <div class="pull-left">
                    <h2>Companies Manage</h2>
                </div>
            </div>
            <div class="col-lg-6 margin-tb">
                <div class="pull-right">
                    @can('user-create')
                    <a class="btn btn-success" href="{{ route('company.create') }}"> Create New Company</a>
                    @endcan
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <br>
        {{--  main  --}}
        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Company</th>
                        <th scope="col">CNPJ</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        @foreach ($companies as $company)
                            <td>{{$company->fantasy_name}}</td>
                            <td>{{$company->registration_number}}</td>
                            <td>
                                <a href="{{route('company.show', $company->id)}}" class="btn btn-info">Show</a>
                                <a href="{{route('company.edit', $company->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                        @endforeach
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
        {{--  finish main  --}}
    </div>
</div>
@endsection
