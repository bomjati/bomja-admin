@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Music</h2>
            </div>
        </div>
        <div class="col-lg-3 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-success" href="{{ route('music.create') }}">New Music</a>
                @endcan
            </div>
        </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <div class="row">
        <div class="col">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Titulo</th>
                        <th scope="col">Artista</th>
                        <th scope="col">Genero</th>
                        <th scope="col">Duração</th>
                        <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($songs as $key => $music)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $music->title }}</td>
                            <td>{{ $music->artist }}</td>
                            <td>{{ $music->genre->name }}</td>
                            <td>{{ $music->totalTime }}</td>
                            <td>
                                <form action="{{ route('music.delete', $music->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')

                                    <a href="{{ route('music.show', $music->id) }}" class="btn btn-info">Show</a>

                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row justify-content-md-center">
                {{ $songs->links() }}
            </div>

        </div>
    </div>

@endsection
