@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Show Music</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-primary" href="{{ route('music.index') }}">Back</a>
                @endcan
            </div>
        </div>
    </div>
    <br>
    <div class="card" style="width: 18rem;">
        <div class="card-header">
           {{$music->title}}
        </div>
        <audio controls>
            <source src="{{$music->url_mp3_s3}}" type="audio/mp3">
            <source src="{{$music->url_aac_s3}}" type="audio/x-aac">
          Your browser does not support the audio element.
          </audio>
    </div>
@endsection
