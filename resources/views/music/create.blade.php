@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Register Music</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-primary" href="{{ route('music.index') }}">Back</a>
                @endcan
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row justify-content-md-center">
        <div class="card" style="width: 25rem;">
            <div class="card-header">
                Musicas
            </div>
            <div class="card-body">
                <form action="{{ route('music.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="col">
                        <div class="form-group">
                            <label for="my-select">Pasta Arcevos</label>
                            <select id="my-select" class="custom-select" name="heap">
                                <option value=" " selected>Selecine</option>
                                @foreach ($heaps as $heap)
                                    <option>{{ $heap->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="my-select">Pasta Gêneros</label>
                            <select id="my-select" class="custom-select" name="genre">
                                <option value=" " selected>Selecine</option>
                                @foreach ($genres as $genre)
                                    <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col p-3">
                        <div class="custom-file">
                            <input type="file" name="music[]" class="custom-file-input" id="music" multiple />
                            <label class="custom-file-label" for="customFile">Musica limite de 20</label>
                        </div>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
            </div>
            </form>
        </div>
    </div>

@endsection
