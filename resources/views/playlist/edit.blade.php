@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Add Music</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-primary" href="{{ route('playlist.index', Auth::id()) }}">Back</a>
                @endcan
            </div>
        </div>
    </div>
    <form action="{{ route('playlist.update', $playlist[0]->playlist->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row justify-content-md-center">
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Nome da Playlist</span>
                    </div>
                    <input type="text" class="form-control" name="name" value="{{ $playlist[0]->playlist->name }}" id="name"
                        placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-6">
                @foreach ($musics as $music)
                    <div class="input-group p-1">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" name="music[]" value="{{ $music->id }}"
                                    aria-label="Checkbox for following text input">
                            </div>
                        </div>
                        <input type="text" class="form-control" value="{{ $music->title }}"
                            aria-label="Text input with radio button" disabled>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row justify-content-md-center p-3">
            <button type="submit" class="btn  btn-primary">Register</button>
        </div>
    </form>
@endsection
