@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Music</h2>
            </div>
        </div>
        <div class="col-lg-3 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-primary" href="{{ route('playlist.index', Auth::id()) }}">back</a>
                @endcan
                @can('user-create')
                    <a class="btn btn-success" href="{{ route('playlist.edit', $playlists[0]->playlist->id) }}">ADD</a>
                @endcan
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    <div class="row justify-content-md-center p-3">
        <div class="col-6">
            <strong>{{ $playlists[0]->playlist->name }}</strong>
            @foreach ($playlists as $playlist)
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" aria-label="Checkbox for following text input">
                        </div>
                    </div>
                    <input type="text" class="form-control" value="{{ $playlist->music->title }}"
                        aria-label="Text input with checkbox" disabled>
                    <form action="{{ route('playlist.delete', $playlist->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="input-group-append" id="button-addon4">
                            <button class="btn btn-outline-secondary material-icons" type="submit">delete</button>
                        </div>
                    </form>

                </div>
            @endforeach

        </div>
    </div>
@endsection
