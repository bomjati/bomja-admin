@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Register Playlist</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-primary" href="{{ route('playlist.index', Auth::id()) }}">Back</a>
                @endcan
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('playlist.store') }}" method="POST">
        @csrf
        <div class="row justify-content-md-center">
            <div class="col-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Nome da Playlist</span>
                    </div>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Username"
                        aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-6">
                @foreach ($songs as $song)
                    <div class="input-group p-1">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" name="music[]" value="{{ $song->id }}"
                                    aria-label="Checkbox for following text input">
                            </div>
                        </div>
                        <input type="text" class="form-control" value="{{ $song->title }}"
                            aria-label="Text input with radio button" disabled>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row justify-content-md-center p-3">
            <button type="submit" class="btn  btn-primary">Register</button>
        </div>
    </form>

@endsection
