@extends('layouts.master')

@section('content')
    <br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2>Playlist</h2>
            </div>
        </div>
        <div class="col-lg-3 margin-tb">
            <div class="pull-right">
                @can('user-create')
                    <a class="btn btn-success" href="{{ route('playlist.create') }}">New Playlist</a>
                @endcan
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('error'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
    <br>
    <div class="row">
        @foreach ($playlists as $playlist)
            <div class="col">
                <div class="card text-center text-white bg-info" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{ $playlist->name }}</h5>

                        <form action="{{ route('playlist.destroy', $playlist->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{ route('playlist.show', $playlist->id) }}"
                                class="btn btn-warning material-icons">view_list</a>
                            <button type="submit" class="btn btn-warning material-icons">delete</button>
                        </form>

                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection
