<?php

namespace App\Models\Media;

use Illuminate\Database\Eloquent\Model;

class ItemPlaylist extends Model
{
    public function playlist()
    {
        return $this->belongsTo(Playlist::class);
    }

    public function music()
    {
        return $this->belongsTo(Music::class);
    }
}
