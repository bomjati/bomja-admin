<?php

namespace App\Models\Media\Cover;

use App\Heap;
use App\Models\Media\Music;
use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $fillable = ['path_s3','genre_id'];

    public function heap()
    {
        return $this->belongsTo(Heap::class);
    }
}
