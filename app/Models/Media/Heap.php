<?php

namespace App\Models\Media;

use App\Models\Media\Cover\Cover;
use App\Models\Media\Genre;
use Illuminate\Database\Eloquent\Model;

class Heap extends Model
{
    protected $fillable = ['name'];

    public function genre()
    {
        return $this->hasOne(Genre::class);
    }
}
