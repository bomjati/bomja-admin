<?php

namespace App\Models\Media;

use App\Models\Media\Cover\Cover;
use Illuminate\Database\Eloquent\Model;

class Music extends Model
{

    protected $fillable = ['title', 'artist', 'totalTime',  'url_aac_s3',  'name_aac', 'genre_id'];

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    public function itemPlaylists()
    {
        return $this->hasMany(Playlist::class);
    }
}
