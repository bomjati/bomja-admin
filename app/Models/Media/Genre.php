<?php

namespace App\Models\Media;

use App\Models\Media\Cover\Cover;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable = ['name'];

    public function heap()
    {
        return $this->belongsTo(Heap::class);
    }

    public function cover()
    {
        return $this->hasOne(Cover::class);
    }

    public function musics()
    {
        return $this->hasMany(Music::class);
    }
}
