<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;


class Company extends Model
{
    protected $fillable = [
        'registration_number', 'business_name', 'fantasy_name',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }

    public function contact()
    {
        return $this->hasOne(Contact::class);
    }

    public function term()
    {
        return $this->hasOne(Term::class);
    }
}
