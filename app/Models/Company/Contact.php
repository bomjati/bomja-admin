<?php

namespace App\Models\Company;


use Illuminate\Database\Eloquent\Model;
use App\Models\Company\Company;

class Contact extends Model
{
    protected $fillable = [
        'email','phone1','phone2'
    ];

    public function company()
    {
        return $this->belongsTo(Comapny::class);
    }
}
