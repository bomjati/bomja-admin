<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company\Company;

class Address extends Model
{
    protected $fillable = [
        'public_place','number','cep','complement','district','county','uf'
    ];

    public function company()
    {
        return $this->belongsTo(Comapny::class);
    }
}
