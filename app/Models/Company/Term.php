<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = ['terms_and_conditions'];

    public function company()
    {
        return $this->belongsTo(Comapny::class);
    }

    public function segment()
    {
        return $this->belongsTo(Segment::class);
    }
}
