<?php

namespace App\Models\Company;

use App\Models\Content\Content;
use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    public function term()
    {
        return $this->hasMany(Term::class);
    }

    public function content()
    {
        return $this->hasOne(Content::class);
    }
}
