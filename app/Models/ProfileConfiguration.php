<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProfileConfiguration extends Model
{
    protected $fillable = [
        'primaryColor',
        'secondaryColor',
        'typeShowcases',
        'offerPhrase',
        'imgName',
        'pathS3',
        'timeToOpen',
        'closeTime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
