<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;


class ProductCategory extends Model
{
    protected $table = "product_categories";
    protected $fillable = ['name'];

    public function product()
    {
        return $this->hasOne(Product::class);
    }

}
