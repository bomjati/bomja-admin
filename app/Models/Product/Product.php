<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'promotion_price',
        'detail',
        'date_start',
        'date_end',
        'time_start',
        'time_end',

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function productImg()
    {
        return $this->hasOne(ProductImg::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function views()
    {
        return $this->hasMany(View::class);
    }
}
