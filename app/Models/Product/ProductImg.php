<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Product;

class ProductImg extends Model
{
    protected $fillable = ['nome_img', 'path_s3', 'product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
