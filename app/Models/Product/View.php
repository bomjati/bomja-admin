<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $fillable = ['view'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
