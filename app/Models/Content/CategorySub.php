<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

class CategorySub extends Model
{
    protected $fillable = ['name'];

    public function category()
    {
        return $this->belongsTo(CategoryContent::class);
    }

    public function content()
    {
        return $this->hasMany(Content::class);
    }
}
