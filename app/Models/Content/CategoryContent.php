<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

class CategoryContent extends Model
{
    protected $fillable = ['name'];

    public function categorySub()
    {
        return $this->hasMany(categorySub::class);
    }
}
