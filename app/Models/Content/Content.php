<?php

namespace App\Models\Content;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = ['name', 'pathS3', 'totalTime', 'timeStart', 'timeEnd'];

    public function categorySub()
    {
        return $this->belongsTo(CategorySub::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }
}
