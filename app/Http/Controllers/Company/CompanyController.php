<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Company\Company;
use App\Models\Company\Address;
use App\Models\Company\Contact;
use App\Models\Company\Segment;
use App\Models\Company\Term;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::with('user','address','contact','term.segment')->get();
        return view('company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $segments = Segment::all();
        return view('company.create', compact('segments'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'registration_number' => 'required',
            'business_name' => 'required',
            'fantasy_name' => 'required',
            'public_place' => 'required',
            'number' => 'required',
            'uf' => 'required',
            'cep' => 'required',
            'district' => 'required',
            'county' => 'required',
            'email' => 'required',
            'phone1' => 'required',
            'segment' => 'required ',
            'term' => 'required '
        ]);

        $id = Auth::id();
        $company = new Company();
        $company->registration_number = $request->registration_number;
        $company->business_name = $request->business_name;
        $company->fantasy_name = $request->fantasy_name;
        $company->user_id = $id;
        $company->save();

        $address = new Address();
        $address->public_place = $request->public_place;
        $address->number = $request->number;
        $address->uf = $request->uf;
        $address->complement = $request->complement;
        $address->cep = $request->cep;
        $address->district = $request->district;
        $address->county = $request->county;
        $address->company_id = $company->id;
        $address->save();

        $contact = new Contact();
        $contact->email = $request->email;
        $contact->phone1 = $request->phone1;
        $contact->phone2 = $request->phone2;
        $contact->company_id = $company->id;
        $contact->save();

        $term = new Term();
        $term->terms_and_conditions = $request->term;
        $term->company_id = $company->id;
        $term->segment_id = $request->segment;
        $term->save();

        return redirect()->route('company.index')->with('success', 'Company created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return view('company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);

        $company->registration_number = $request->registration_number;
        $company->business_name = $request->business_name;
        $company->fantasy_name = $request->fantasy_name;
        $company->legal_nature_code = $request->legal_nature_code;
        $company->address->public_place = $request->public_place;
        $company->address->number = $request->number;
        $company->address->uf = $request->uf;
        $company->address->complement = $request->complement;
        $company->address->district = $request->district;
        $company->address->county = $request->county;
        $company->contact->email = $request->email;
        $company->contact->phone1 = $request->phone1;
        $company->contact->phone2 = $request->phone2;

        $company->push();

        return redirect()->route('company.index')->with('success', 'Company update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
