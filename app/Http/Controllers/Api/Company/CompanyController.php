<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Company\Company;
use App\Models\Company\Address;
use Illuminate\Support\Facades\Validator;
use App\Models\Company\Contact;
use App\Models\Company\Segment;
use App\Models\Company\Term;

class CompanyController extends Controller
{
    public function index()
    {
        $id = Auth::id();

        $company =  Company::where('user_id', $id)->with('user', 'address', 'contact', 'term.segment')->get();

        return response()->json($company);
    }

    public function segment()
    {
        $segments = Segment::all();
        return response()->json($segments);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'registration_number' => 'required',
            'business_name' => 'required',
            'fantasy_name' => 'required',
            'public_place' => 'required',
            'number' => 'required',
            'uf' => 'required',
            'cep' => 'required',
            'district' => 'required',
            'county' => 'required',
            'email' => 'required',
            'phone1' => 'required',
            'segment' => 'required',
            'terms_and_conditions' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $id = Auth::id();

        $company = new Company();
        $company->registration_number = $request->registration_number;
        $company->business_name = $request->business_name;
        $company->fantasy_name = $request->fantasy_name;
        $company->user_id = $id;
        $company->save();

        $address = new Address();
        $address->public_place = $request->public_place;
        $address->number = $request->number;
        $address->uf = $request->uf;
        $address->complement = $request->complement;
        $address->cep = $request->cep;
        $address->district = $request->district;
        $address->county = $request->county;
        $address->company_id = $company->id;
        $address->save();

        $contact = new Contact();
        $contact->email = $request->email;
        $contact->phone1 = $request->phone1;
        $contact->phone2 = $request->phone2;
        $contact->company_id = $company->id;
        $contact->save();

        $term = new Term();
        $term->terms_and_conditions = $request->terms_and_conditions;
        $term->company_id = $company->id;
        $term->segment_id = $request->segment;
        $term->save();

        return response()->json([
            'message' => 'User successfully registered'
        ], 200);
    }

    public function show($id)
    {
        $company = Company::find($id);
        $company->load('address', 'contact');


        return response()->json($company);
    }

    public function update(Request $request, $id)
    {
        $company = Company::find($id);

        $company->registration_number = $request->registration_number;
        $company->business_name = $request->business_name;
        $company->fantasy_name = $request->fantasy_name;
        $company->address->public_place = $request->public_place;
        $company->address->number = $request->number;
        $company->address->uf = $request->uf;
        $company->address->complement = $request->complement;
        $company->address->cep = $request->cep;
        $company->address->district = $request->district;
        $company->address->county = $request->county;
        $company->contact->email = $request->email;
        $company->contact->phone1 = $request->phone1;
        $company->contact->phone2 = $request->phone2;

        $company->push();

        return response()->json([
            'message' => 'Information changed successfully'
        ], 201);
    }

    public function destroy($id)
    {
        $company = Company::findOrFail($id);

        $company->delete();

        return response()->json([
            'message' => 'Information deleted successfully'
        ], 201);
    }
}
