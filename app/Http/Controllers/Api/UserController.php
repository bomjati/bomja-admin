<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::All();
        return response()->json($users);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password', 'min:6',
            'roles' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);


            $user = User::create(array_merge(
                $validator->validated(),
                ['password' => bcrypt($request->password)]
            ));

            $user->assignRole($request->input('roles'));

            return response()->json([
                'message' => 'User successfully registered',
                'user' => $user
            ], 201);
        }
    }

    public function show($id)
    {
        return User::find($id);
    }


    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'required|string|confirmed|min:6',
            'roles' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        // dd($validator);
        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));

        return response()->json([
            'message' => 'Information changed successfully',
        ], 201);
    }


    public function destroy($id)
    {
        User::find($id)->delete();

        return response()->json([
            'message' => 'Information deleted successfully',
        ], 201);
    }
}
