<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\Comment;
use App\Models\Product\Like;
use App\Models\Product\Product;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NetController extends Controller
{
    public function like(Request $request, $id)
    {
        $loggedInUser = Auth::id();
        $like = new Like();
        $likes = Like::where(['product_id' => $id, 'user_id' => $loggedInUser])->get();
        $product = Product::find($id);

        if ($likes->count() > 0) {
            if ($request->like == 1) {
                DB::table('likes')->where('id', $likes[0]->id)->update(['like' => 1, 'status' => true]);
                return response()->json([
                    'message' => 'You liked it again'
                ], 200);
            } else {
                DB::table('likes')->where('id', $likes[0]->id)->update(['like' => 0, 'status' => false]);
                return response()->json([
                    'message' => 'You stopped liking'
                ], 200);
            }
        }
        $like->like = $request->like;
        $like->status = true;
        $like->user_id =  $loggedInUser;
        $like->product_id = $product->id;
        $like->save();

        return response()->json([
            'message' => 'Liked it'
        ], 200);
    }

    public function view($id)
    {
        $product = Product::find($id);
        $exist = DB::table('views')->where('product_id', $product->id)->exists();

        if (!$exist) {
            DB::table('views')->insert([
                'view' => 1,
                'product_id' => $product->id,
            ]);
            return response()->json([
                'message' => 'first view record'
            ], 200);
        } else {
            return response()->json([
                'message' => 'user already viewed'
            ], 200);
        }
    }

    public function create(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $userLogin = Auth::id();
        $product = Product::find($id);
        $comment = new Comment();

        $comment->comment = $request->comment;
        $comment->user_id = $userLogin;
        $comment->product_id = $product->id;
        $comment->save();

        return response()->json([
            'message' => 'Comment successfully created'
        ], 200);
    }

    public function edit(Request $request, $id)
    {
        $UserId = Auth::id();
        $comment = Comment::where('id', $id)->with('user')->get();
        $userCommentId = $comment[0]->user->id;

        if ($userCommentId == $UserId && $comment[0]->id == $id) {
            DB::table('comments')->where('id', $id)->update(['comment' => $request->comment]);
            return response()->json([
                'message' => 'Comment successfully changed'
            ], 200);
        } else {
            return response()->json([
                'message' => 'You can t change'
            ], 200);
        }
    }

    public function destroy($id)
    {
        $UserId = Auth::id();
        $comment = Comment::where('id', $id)->with('user')->get();
        $userCommentId = $comment[0]->user->id;

        if ($userCommentId == $UserId && $comment[0]->id == $id) {
            DB::table('comments')->where('id', $id)->delete();
            return response()->json([
                'message' => 'Review deleted successfully'
            ], 200);
        } else {
            return response()->json([
                'message' => 'You cannot delete'
            ], 200);
        }
    }
}
