<?php

namespace App\Http\Controllers\Api\Product;

use App\Helpers\UploadImg;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index($id)
    {
        $data = array();

        $user = ($id == '0') ? Auth::id() : $id;

        $products = Product::where('user_id', $user)->with('user', 'productImg')->get();

        foreach ($products as $product) {

            $like = $product->likes->sum('like');
            $comment = $product->comments->count();
            $data[]  = ['product' => $product->makeHidden(['likes', 'comments']), 'like' => $like, 'comment' => $comment];
        }

        return response()->json($data);
    }

    public function info($id)
    {
        $product = Product::where('id', $id)->with('user', 'productImg', 'likes.user', 'comments.user')->get();

        return response()->json($product);
    }

    public function category()
    {
        $category = ProductCategory::all();
        return response()->json($category);
    }
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'promotion_price' => 'required',
            'detail' => 'required',
            'category' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
            'img' => 'required|mimes:jpeg,png'

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $file = $request->file('img');
        $name_img = date('dmyHis') . '.png';

        $prod_category = ProductCategory::find($request->category);
        $category = preg_replace('/[ -]+/', '-', $prod_category->name);

        // diretorio para salvar na s3 pelo storage do laravel
        $path = 'images/produtos/' . $category . '/' . $name_img;

        //diretorio para salvar no s3 pelo cli da aws usando shell_exec
        $path_s3 = '/images/produtos' . '/' . $category;

        list($width, $height, $type) = getimagesize($file);


        if ($width >= 500 && $height >= 1000 && $type == 3) {

            $img_png = imagecreatefrompng($file);
            $transparency = imagecolorexactalpha($img_png, 0, 0, 0, 127);

            if ($transparency == -1 || $transparency == 2130706432) {

                Storage::disk('s3')->put($path, file_get_contents($file), 'public');
            }
        } elseif ($type == 3) {
            UploadImg::bottomlessPNG($file, $name_img, $path_s3);
        } else {
            UploadImg::jpegToPng($file, $name_img, $path_s3);
        }

        $id = Auth::id();
        $products = new Product();

        $products->name = $request->name;
        $products->price = $request->price;
        $products->promotion_price = $request->promotion_price;
        $products->detail = $request->detail;
        $products->date_start = $request->date_start;
        $products->date_end = $request->date_end;
        $products->time_start = $request->time_start;
        $products->time_end = $request->time_end;
        $products->user_id = $id;
        $products->product_category_id = $request->category;
        $products->save();

        ProductImg::create([

            'nome_img' => $name_img,
            'path_s3' => Storage::disk('s3')->url($path),
            'product_id' => $products->id

        ]);

        return response()->json([
            'message' => 'User successfully registered'
        ], 200);
    }


    public function update(Request $request, $id)
    {
        $name_img = date('dmyHis') . '.png';

        $prod_category = ProductCategory::find($request->category);
        $category = preg_replace('/[ -]+/', '-', $prod_category->name);
        // diretorio para salvar na s3 pelo storage do laravel
        $path = 'images/produtos/' . $category . '/' . $name_img;

        $product = Product::find($id);

        if (!($request->img == "sem imagem")) {

            UploadImg::deleteImg($id);

            $img = $request->file('img');

            list($width, $height, $type) = getimagesize($img);

            if ($width >= 500 && $height >= 1000 && $type == 3) {

                $img_png = imagecreatefrompng($img);
                $transparency = imagecolorexactalpha($img_png, 0, 0, 0, 127);

                if ($transparency == -1) {
                    Storage::disk('s3')->put($path, file_get_contents($img), 'public');
                }
            } elseif ($type == 3) {

                UploadImg::bottomlessPNG($img, $category, $name_img);
            } else {
                UploadImg::jpegToPng($img, $category, $name_img);
            }

            $product->productImg->nome_img = $name_img;
            $product->productImg->path_s3 = Storage::disk('s3')->url($path);
        }

        $product->name = $request->name;
        $product->price = $request->price;
        $product->promotion_price = $request->promotion_price;
        $product->detail = $request->detail;
        $product->date_start = $request->date_start;
        $product->date_end = $request->date_end;
        $product->time_start = $request->time_start;
        $product->time_end = $request->time_end;
        $product->product_category_id = $request->category;

        $product->push();

        return response()->json([
            'message' => 'Information changed successfully'
        ], 200);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $category = $product->productCategory->name;
        $img = $product->productImg->nome_img;

        $category = preg_replace('/[ -]+/', '-',  $category);

        $products_s3 = Storage::disk('s3')->exists('images/produtos/' . $category . '/' . $img);

        if ($products_s3) {

            Storage::disk('s3')->delete('images/produtos/' . $category . '/' . $img);

            $product->delete();

            return response()->json([
                'message' => 'Information deleted successfully'
            ], 200);
        } else {
            $product->delete();
            return response()->json([
                'message' => 'Product not exists'
            ], 412);
        }
    }
}
