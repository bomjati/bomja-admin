<?php

namespace App\Http\Controllers\Api;

use App\Helpers\UploadImg;
use App\Http\Controllers\Controller;
use App\Models\ProfileConfiguration;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DisableUserController extends Controller
{

    public function index(User $user)
    {
        $userTrashed = $user->onlyTrashed()->get();
        return response()->json($userTrashed);
    }

    public function restore($id)
    {
        // Recupera o post pelo ID
        User::onlyTrashed()->where('id', $id)->restore();

        return response()->json([
            'message' => 'Account Successfully Restored'
        ], 200);
    }


    public function configIndex($id)
    {
        $user = ($id == '0') ? Auth::id() : $id;

        $config = ProfileConfiguration::where('user_id', $user)->with('user')->get();

        return response()->json($config);
    }

    public function configProfile(Request $request)
    {
        $config = new ProfileConfiguration();

        if (!($request->img == "sem imagem")) {

            $file = $request->file('img');
            $nameImg = date('dmyHis') . '-' . $file->getClientOriginalName();

            $path = 'images/photoProfile/' . $nameImg;

            Storage::disk('s3')->put($path, file_get_contents($file), 'public');

            $config->imgName = $nameImg;
            $config->pathS3 = Storage::disk('s3')->url($path);
        }

        $config->primaryColor = $request->primaryColor;
        $config->secondaryColor = $request->secondaryColor;
        $config->typeShowcases = $request->typeShowcases;
        $config->offerPhrase = $request->offerPhrase;
        $config->timeToOpen = $request->timeToOpen;
        $config->closeTime = $request->closeTime;
        $config->user_id = Auth::id();
        $config->save();

        return response()->json([
            'message' => 'Saved successfully'
        ], 200);
    }

    public function configUpdate(Request $request, $id)
    {
        $config = ProfileConfiguration::find($id);

        if (!($request->img == "sem imagem")) {

            UploadImg::deletePicture($id);

            $file = $request->file('img');
            $nameImg = date('dmyHis') . '-' . $file->getClientOriginalName();

            $path = 'images/photoProfile/' . $nameImg;

            Storage::disk('s3')->put($path, file_get_contents($file), 'public');

            $config->imgName = $nameImg;
            $config->pathS3 = Storage::disk('s3')->url($path);
        }

        $params = $request->all();
        $config->update($params);

        return response()->json([
            'message' => 'Change made successfully'
        ], 200);
    }
}
