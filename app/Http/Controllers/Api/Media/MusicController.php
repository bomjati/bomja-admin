<?php

namespace App\Http\Controllers\Api\Media;

use App\Http\Controllers\Controller;
use App\Models\Media\Genre;
use App\Models\Media\ItemPlaylist;
use App\Models\Media\Playlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MusicController extends Controller
{
    public function index()
    {
        $res = Genre::all();
        $genres = $res->load('heap', 'cover', 'musics');
        return response()->json($genres);
    }

    public function playlistIndex()
    {
        $playlists = Playlist::where('user_id', Auth::id())->with('itemPlaylists.music.genre.cover')->get();
        return response()->json($playlists);
    }

    public function playlistStore(Request $request)
    {

        $playlist = new Playlist();

        $playlist->name = $request->name;
        $playlist->user_id = Auth::id();
        $playlist->save();

        $songs = $request->music;
        foreach ($songs as $song) {
            $itemPlaylist = new ItemPlaylist();
            $itemPlaylist->playlist_id = $playlist->id;
            $itemPlaylist->music_id = $song;
            $itemPlaylist->save();
        }

        return response()->json([
            'message' => 'playlist successfully created'
        ], 200);
    }

    public function playlistUpdate(Request $request, $id)
    {
        $playlist = Playlist::find($id);
        $playlist->name = $request->name;
        $playlist->push();

        if (!($request->music == 'vazio')) {

            $songs = $request->music;
            foreach ($songs as $song) {
                $itemPlaylist = new ItemPlaylist();
                $itemPlaylist->playlist_id = $playlist->id;
                $itemPlaylist->music_id = $song;
                $itemPlaylist->save();
            }
        }
        return response()->json([
            'message' => 'Playlist saved successfully'
        ], 200);
    }

    public function playlistDestroy($id)
    {
        $playlist = Playlist::find($id);

        $exist = DB::table('playlists')->where(['id' =>  $id, 'user_id' => Auth::id()])->exists();
        if ($exist) {
            $playlist->delete();
            return response()->json([
                'message' => 'Playlist deleted successfully'
            ], 200);
        }
        return response()->json([
            'message' => 'The playlist does not exist or has already been deleted'
        ], 500);
    }
    public function itemDestroy($id)
    {
        $item = ItemPlaylist::find($id);

        $exist = DB::table('item_playlists')->where(['id' => $id, 'playlist_id' => $item->playlist_id, 'music_id' => $item->music_id])->exists();
        if ($exist) {
            $item->delete();
            return response()->json([
                'message' => 'Item deleted successfully'
            ], 200);
        }
        return response()->json([
            'message' => 'The item does not exist or has already been deleted'
        ], 500);
    }
}
