<?php

namespace App\Http\Controllers\Api\Media;

use App\Helpers\UploadMusic;
use App\Http\Controllers\Controller;
use App\Models\Content\CategorySub;
use App\Models\Content\Content;
use App\Models\Content\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    public function indexServe()
    {
        $contents = Content::where('user_id', Auth::id())->with('categorySub.category', 'user')->get();
        return response()->json($contents);
    }

    public function indexClient()
    {
        dd('index client');
        return response()->json();
    }

    public function storeEmployee(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $allowedfileExtension = ['mp3'];
        $file = $request->file('content');

        $extension = $file->getClientOriginalExtension();
        $check = in_array($extension, $allowedfileExtension);

        if ($check) {
            $data = UploadMusic::contentClient($request->name, $file);
        } else {

            return response()->json([
                'message' => 'The file is not mp3'
            ], 500);
        }

        $employee = new Employee();
        $employee->nomeEmployee = $request->name;
        $employee->name = $data[0];
        $employee->pathS3 = Storage::disk('s3')->url($data[1]);
        $employee->company_id = $data[2];
        $employee->save();

        return response()->json([
            'message' => 'Saved successfully'
        ], 200);
    }

    public function storeContent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'segment' => 'required',
            'subCategory' => 'required',
            'dateStart' => 'required',
            'timeStart' => 'required',
            'dateEnd' => 'required',
            'timeEnd' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $subCategory = new CategorySub();
        $subCategory->name = $request->subCategory;
        $subCategory->category_id = 7;
        $subCategory->save();

        $allowedfileExtension = ['mp3'];
        $files = $request->file('content');

        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            if ($check) {
                $data = UploadMusic::contentClient($request->subCategory, $file);
            } else {
                return response()->json([
                    'message' => 'The file is not mp3'
                ], 500);
            }

            $content  = new Content();
            $content->name = $data[0];
            $content->pathS3 = Storage::disk('s3')->url($data[1]);
            $content->totalTime = $data[3];
            $content->timeStart = $request->dateStart . '-' . $request->timeStart;
            $content->timeEnd = $request->dateEnd . '-' . $request->timeEnd;
            $content->user_id = auth::id();
            $content->category_sub_id = $subCategory->id;
            $content->segment_id = $request->segment;
            $content->save();
        }

        return response()->json([
            'message' => 'Saved successfully'
        ], 200);
    }

    public function destroy($id)
    {
        $content = Content::find($id);
        Content::where(['id' => $content->id, 'user_id' => $id])->delete();

        return response()->json([
            'message' => 'Deleted successfully'
        ], 200);
    }
}
