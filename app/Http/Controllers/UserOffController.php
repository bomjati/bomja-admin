<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class UserOffController extends Controller
{
    function __construct(){
        $this->middleware('permission:off-list');
        $this->middleware('permission:off-restore');
    }

    public function index(User $user){

        $userTrashed = $user->onlyTrashed()->get();
        return view('offline.index', compact('userTrashed'));
    }

    public function restore(User $user, $id){
        // Recupera o post pelo ID
        $user = User::onlyTrashed()->where('id', $id)->restore();

        return redirect()->route('offline.index')->with('success','User restore successfully');
    }
}
