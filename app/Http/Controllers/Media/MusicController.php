<?php

namespace App\Http\Controllers\Media;

use App\Helpers\Help;
use App\Helpers\UploadMusic;
use App\Http\Controllers\Controller;
use App\Models\Media\Genre;
use App\Models\Media\Heap;
use App\Models\Media\ItemPlaylist;
use App\Models\Media\Music;
use App\Models\Media\Playlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class MusicController extends Controller
{
    public function index(Request $request)
    {
        $songs = Music::orderBy('id', 'DESC')->paginate(5);
        return view('music.index', compact('songs'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $heaps = Heap::all();
        $genres = Genre::all();

        return view('music.create', compact('heaps', 'genres'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'heap' => 'required',
            'genre' => 'required',
            'music' => 'required'
        ]);

        $allowedfileExtension = ['mp3'];
        $files = $request->file('music');
        $genres = Genre::find($request->genre);

        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);

            if ($check) {
                UploadMusic::saveMusic($file, $request->heap, $genres->name, $genres->id);
            } else {
                return redirect()->route('music.create')->with('error', 'The file is not mp3');
            }
        }
        return redirect()->route('music.index')->with('success', 'Sound saved successfully');
    }

    public function show($id)
    {
        $music = Music::findOrFail($id);
        return view('music.show', compact('music'));
    }

    public function playlistIndex($id)
    {
        $playlists = Playlist::where('user_id', $id)->get();
        return view('playlist.index', compact('playlists'));
    }

    public function playlistCreate()
    {
        $songs = Music::all();
        return view('playlist.create', compact('songs'));
    }

    public function playlistEdit($id)
    {
        $playlist = ItemPlaylist::where('playlist_id', $id)->with('playlist', 'music')->get();
        $music = Music::all();

        $musics = Help::removeFromList($playlist, $music);

        return view('playlist.edit', compact('playlist', 'musics'));
    }

    public function playlistStore(Request $request)
    {
        $playlist = new Playlist();

        $playlist->name = $request->name;
        $playlist->user_id = Auth::id();
        $playlist->save();

        $songs = $request->music;
        foreach ($songs as $song) {
            $itemPlaylist = new ItemPlaylist();
            $itemPlaylist->playlist_id = $playlist->id;
            $itemPlaylist->music_id = $song;
            $itemPlaylist->save();
        }

        return redirect()->route('playlist.index', Auth::id())->with('success', 'Playlist saved successfully');
    }

    public function playlistUpdate(Request $request, $id)
    {
        $playlist = ItemPlaylist::where('playlist_id', $id)->with('playlist')->get();

        $playlist[0]->playlist->name = $request->name;
        $playlist[0]->push();

        $songs = $request->music;
        foreach ($songs as $song) {
            $itemPlaylist = new ItemPlaylist();
            $itemPlaylist->playlist_id = $playlist[0]->playlist->id;
            $itemPlaylist->music_id = $song;
            $itemPlaylist->save();
        }

        return redirect()->route('playlist.show', $id)->with('success', 'Playlist saved successfully');
    }

    public function playlistShow($id)
    {
        $playlists = ItemPlaylist::where('playlist_id', $id)->with('playlist', 'music')->get();
        return view('playlist.show', compact('playlists'));
    }

    public function playlistDestroy($id)
    {
        $playlist = Playlist::find($id);
        $exist = DB::table('playlists')->where(['id' =>  $id, 'user_id' => Auth::id()])->exists();

        if ($exist) {
            $playlist->delete();
            return redirect()->route('playlist.index', Auth::id())->with('success', 'Playlist deleted successfully');
        }
        return redirect()->route('playlist.index', Auth::id())->with('error', 'The playlist does not exist or has already been deleted');
    }
    public function itemDestroy($id)
    {
        $item = ItemPlaylist::find($id);

        $exist = DB::table('item_playlists')->where(['id' => $id, 'playlist_id' => $item->playlist_id, 'music_id' => $item->music_id])->exists();

        if ($exist) {
            $item->delete();
            return redirect()->route('playlist.show', $item->playlist_id)->with('success', 'Item deleted successfully');
        }

        return redirect()->route('playlist.show', $item->playlist_id)->with('error', 'The item does not exist or has already been deleted');
    }
    public function delete($id)
    {
        $music = Music::findOrFail($id);
        $res = Genre::where('id', $music->genre_id)->with('heap')->get();
        $acervo = $res[0]->heap->name;

        $mp3_s3 = Storage::disk('s3')->exists('sounds/' . $acervo . '/' . $music->genre->name . '/', $music->name_mp3);

        $aac_s3 = Storage::disk('s3')->exists('sounds/' . $acervo . '/' . $music->genre->name . '/', $music->name_aac);

        if ($mp3_s3 && $aac_s3) {

            Storage::disk('s3')->delete('sounds/' . $acervo  . '/' . $music->genre->name . '/' . $music->name_mp3);

            Storage::disk('s3')->delete('sounds/' . $acervo  . '/' . $music->genre->name . '/' . $music->name_aac);

            $music->delete();

            return redirect()->route('music.index')->with('success', 'Sound deleted successfully');
        } else {

            return redirect()->route('music.index')->with('success', 'Error:Sound deleted successfully');
        }
    }
}
