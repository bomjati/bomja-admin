<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Controller;
use App\Models\Media\Cover\Cover;
use App\Models\Media\Genre;
use App\Models\Media\Heap;
use App\Models\Media\Music;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HeapController extends Controller
{
    public function index()
    {
        $res = Genre::all();
        $genres = $res->load('heap', 'cover');

        return view('heap.index', compact('genres'));
    }

    public function create()
    {
        $heaps = Heap::all();
        return view('heap.create', compact('heaps'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'acervo' => 'required',
            'genre' => 'required',
            'img' => 'required|mimes:jpeg,png|dimensions:min_width=500,min_height=500'
        ]);

        if ($request->hasFile('img')) {
            $acervo = Heap::find($request->acervo);
            $file = $request->file('img');
            $name = $file->getClientOriginalName();
            $filePath = 'sounds/' . $acervo->name . '/' . $request->genre . '/' . $name;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
        }

        $genre = new Genre();
        $genre->name = $request->genre;
        $genre->heap_id = $request->acervo;
        $genre->save();

        Cover::create([
            'path_s3' => Storage::disk('s3')->url($filePath),
            'genre_id' => $genre->id
        ]);


        return redirect()->route('heap.index')->with('success', 'Genre created successfully');
    }

    public function show($id)
    {
        $genre = Music::where('genre_id', $id)->get();
        return view('heap.show', compact('genre'));
    }
}
