<?php

namespace App\Http\Controllers\Content;

use App\Helpers\UploadMusic;
use App\Http\Controllers\Controller;
use App\Models\Company\Segment;
use App\Models\Content\CategoryContent;
use App\Models\Content\CategorySub;
use App\Models\Content\Content;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    public function index()
    {
        $contents = Content::with('categorySub.category', 'user')->get();
        // dd($contents);
        return view('content.index', compact('contents'));
    }

    public function create()
    {
        $segments = Segment::all();
        $categories = CategoryContent::all();
        $users = User::all();
        return view('content.create', compact('segments', 'categories', 'users'));
    }
    public function store(Request $request)
    {
        dd($request->all());
        $request->validate([
            'segment' => 'required',
            'category' => 'required',
            'subCategory' => 'required',
            'user' => 'required',
            'dateStart' => 'required',
            'timeStart' => 'required',
            'dateEnd' => 'required',
            'timeEnd' => 'required',
            'music' => 'required'
        ]);

        $subCategory = new CategorySub();
        $subCategory->name = $request->subCategory;
        $subCategory->category_id = $request->category;
        $subCategory->save();

        $allowedfileExtension = ['mp3'];
        $files = $request->file('music');

        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            if ($check) {
                $data = UploadMusic::saveContent($file, $request->category, $request->subCategory);
            } else {
                return redirect()->route('content.create')->with('error', 'The file is not mp3');
            }

            foreach ($request->user as $user) {
                $content  = new Content();
                $content->name = $data[0];
                $content->pathS3 = Storage::disk('s3')->url($data[1]);
                $content->totalTime = $data[2];
                $content->timeStart = $request->dateStart . '-' . $request->timeStart;
                $content->timeEnd = $request->dateEnd . '-' . $request->timeEnd;
                $content->user_id = $user;
                $content->category_sub_id = $subCategory->id;
                $content->segment_id = $request->segment;
                $content->save();
            }
        }
        return redirect()->route('content.index')->with('sucess', 'Content saved successfully');
    }

    public function destroy($id)
    {
        Content::where('user_id', $id)->delete();
        return redirect()->route('content.index')->with('sucess', 'Content deleted successfully');
    }
}
