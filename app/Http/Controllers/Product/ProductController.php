<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Helpers\UploadImg;
use App\Models\Product\Comment;
use App\Models\Product\Like;
use App\Models\Product\View;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function index()
    {
        $res = Product::all();
        $products = $res->load('productCategory', 'productImg');

        return view('products.index', compact('products'));
    }

    public function create()
    {
        $categories = ProductCategory::all();
        return view('products.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'promotion_price' => 'required',
            'detail' => 'required',
            'category' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
            'img' => 'required|mimes:jpeg,png'

        ]);

        $file = $request->file('img');
        $name_img = date('dmyHis') . '.png';

        $prod_category = ProductCategory::find($request->category);
        $category = preg_replace('/[ -]+/', '-', $prod_category->name);

        // diretorio para salvar na s3 pelo storage do laravel
        $path = 'images/produtos/' . $category . '/' . $name_img;

        //diretorio para salvar no s3 pelo cli da aws usando shell_exec
        $path_s3 = '/images/produtos' . '/' . $category;

        list($width, $height, $type) = getimagesize($file);


        if ($width >= 500 && $height >= 1000 && $type == 3) {

            $img_png = imagecreatefrompng($file);
            $transparency = imagecolorexactalpha($img_png, 0, 0, 0, 127);

            if ($transparency == -1 || $transparency == 2130706432) {

                Storage::disk('s3')->put($path, file_get_contents($file), 'public');
            }
        } elseif ($type == 3) {
            UploadImg::bottomlessPNG($file, $name_img, $path_s3);
        } else {
            UploadImg::jpegToPng($file, $name_img, $path_s3);
        }

        $id = Auth::id();
        $products = new Product();

        $products->name = $request->name;
        $products->price = $request->price;
        $products->promotion_price = $request->promotion_price;
        $products->detail = $request->detail;
        $products->date_start = $request->date_start;
        $products->date_end = $request->date_end;
        $products->time_start = $request->time_start;
        $products->time_end = $request->time_end;
        $products->user_id = $id;
        $products->product_category_id = $request->category;
        $products->save();

        ProductImg::create([

            'nome_img' => $name_img,
            'path_s3' => Storage::disk('s3')->url($path),
            'product_id' => $products->id

        ]);


        return redirect()->route('product.index')->with('success', 'Product saved successfully');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = ProductCategory::all();
        return view('products.edit', compact('product', 'categories'));
    }
    public function update(Request $request, $id)
    {
        if ($request->file('img')) {
            UploadImg::deleteImg($id);

            $img = $request->file('img');
            $name_img = date('dmyHis') . '.png';
            $prod_category = ProductCategory::find($request->category);
            $category = preg_replace('/[ -]+/', '-', $prod_category->name);
            // diretorio para salvar na s3 pelo storage do laravel
            $path = 'images/produtos/' . $category . '/' . $name_img;

            //diretorio para salvar no s3 pelo cli da aws usando shell_exec
            $path_s3 = '/images/produtos' . '/' . $category;

            list($width, $height, $type) = getimagesize($img);

            if ($width >= 500 && $height >= 1000 && $type == 3) {

                $img_png = imagecreatefrompng($img);
                $transparency = imagecolorexactalpha($img_png, 0, 0, 0, 127);

                if ($transparency == -1 || $transparency == 2130706432) {
                    Storage::disk('s3')->put($path, file_get_contents($img), 'public');
                }
            } elseif ($type == 3) {
                UploadImg::bottomlessPNG($img, $name_img, $path_s3);
            } else {
                UploadImg::jpegToPng($img,  $name_img, $path_s3);
            }
        }

        $product = Product::find($id);

        $product->name = $request->name;
        $product->price = $request->price;
        $product->promotion_price = $request->promotion_price;
        $product->detail = $request->detail;
        $product->date_start = $request->date_start;
        $product->date_end = $request->date_end;
        $product->time_start = $request->time_start;
        $product->time_end = $request->time_end;
        $product->product_category_id = $request->category;

        $product->productImg->nome_img = $name_img;
        $product->productImg->path_s3 = Storage::disk('s3')->url($path);

        $product->push();

        return redirect()->route('product.index')->with('success', 'Product update successfully');
    }

    public function show($id)
    {
        $product = Product::where('id', $id)->with('productImg', 'like')->get();

        $cont = Like::where('product_id', $product[0]->id)->get();
        $like = $cont->sum('like');

        $comments = Comment::where('product_id', $product[0]->id)->with('user')->get();

        return view('products.show', compact('product', 'like', 'comments'));
    }

    public function like(Request $request, $id)
    {
        $loggedInUser = Auth::id();
        $like = new Like();
        $likes = Like::where(['product_id' => $id, 'user_id' => $loggedInUser])->get();
        $product = Product::find($id);

        if ($likes->count() > 0) {
            if ($request->like == 1) {
                DB::table('likes')->where('id', $likes[0]->id)->update(['like' => 1, 'status' => true]);
                return redirect()->route('product.show', $id);
            } else {
                DB::table('likes')->where('id', $likes[0]->id)->update(['like' => 0, 'status' => false]);
                return redirect()->route('product.show', $id);
            }
        }
        $like->like = $request->like;
        $like->status = true;
        $like->user_id =  $loggedInUser;
        $like->product_id = $product->id;
        $like->save();

        return redirect()->route('product.show', $id);
    }

    public function view($id)
    {
        $userId = Auth::id();
        $product = Product::find($id);
        $exist = DB::table('views')->where('user_id', $userId)->exists();
        $view = new View();

        if (!$exist) {
            DB::table('views')->insert([
                'view' => 1,
                'user_id' => $userId,
                'product_id' => $product->id,
            ]);
            return redirect()->route('product.index');
        } else {
            return redirect()->route('product.index');
        }
    }

    public function comment(Request $request, $id)
    {
        $request->validate([
            'comment' => 'required',

        ]);

        $userLogin = Auth::id();
        $product = Product::find($id);
        $comment = new Comment();

        $comment->comment = $request->comment;
        $comment->user_id = $userLogin;
        $comment->product_id = $product->id;
        $comment->save();

        return redirect()->route('product.show', $id);
    }

    public function commentEdit($id)
    {
        $UserId = Auth::id();
        $comment = Comment::where('id', $id)->with('user')->get();
        $userCommentId = $comment[0]->user->id;

        if ($userCommentId == $UserId && $comment[0]->id == $id) {
            return view('products.editComment', compact('comment'));
        } else {
            return back()->with('error', 'Você não pode alterar');
        }
    }

    public function commentUpdate(Request $request, $id)
    {

        $comment = Comment::find($id);
        $comment->comment = $request->comment;
        $comment->push();

        return redirect()->route('product.show', $comment->product_id);
    }
    public function commentDestroy($id)
    {
        $UserId = Auth::id();
        $comment = Comment::where('id', $id)->with('user')->get();
        $userCommentId = $comment[0]->user->id;

        if ($userCommentId == $UserId && $comment[0]->id == $id) {
            DB::table('comments')->where('id', $id)->delete();
            return redirect()->route('product.show', $comment[0]->product_id);
        } else {
            return back()->with('error', 'Você não pode deletar');
        }
    }
    public function destroy($id)
    {
        $product = Product::find($id);
        $category = $product->productCategory->name;
        $img = $product->productImg->nome_img;

        $category = preg_replace('/[ -]+/', '-',  $category);

        $products_s3 = Storage::disk('s3')->exists('images/produtos/' . $category . '/' . $img);

        if ($products_s3) {

            Storage::disk('s3')->delete('images/produtos/' . $category . '/' . $img);

            $product->delete();

            return redirect()->route('product.index')->with('success', 'Product deleted successfully');
        } else {
            $product->delete();
            return redirect()->route('product.index')->with('success', 'Error:Product not exists');
        }
    }
}
