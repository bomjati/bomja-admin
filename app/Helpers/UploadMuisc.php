<?php

namespace App\Helpers;

use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\Company\Term;
use App\Models\Content\CategoryContent;
use App\Models\Media\Music;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class UploadMusic extends Controller
{
    static protected $path =   '/var/www/html/assets/';

    public static function saveMusic($file, $heap, $genres, $genresId)
    {
        $filename = date('dmyHis') . "-" . $file->getClientOriginalName();
        list($id, $artists, $titles, $genre) = explode("-", $filename);

        $id = trim($id, " ");
        $artist =  trim($artists, " ");
        $title = trim($titles, " ");

        if ($genres == $genre) {
            #formatação do nome comprimir e salvar
            $nameMP3 = $id . '-' . $title . '-' . $artist;
            $nameAAC = preg_replace('/[ -]+/', '-', $nameMP3);

            # nome para salvar no banco
            # $mp3 = $nameMP3 . '.mp3';
            $aac = $nameAAC . '.aac';

            #dir para slavar na s3 pelo cli
            $path_s3 = '/sounds' . '/' . $heap . '/' . $genres;
            #dir para salvar na s3 pelo driver laravel
            # $filePathMp3 = 'sounds/' . $heap . '/' . $genres . '/' . $mp3;
            #dir para salvar na s3 pelo driver laravel
            $filePathAac = 'sounds/' . $heap . '/' . $genres . '/' . $aac;

            #comprimi em aac
            exec('ffmpeg -i ' . $file .  ' -c:a aac -b:a 64k ' . static::$path . $aac . ' 2>&1 1> /dev/null');

            $fileTmp = static::$path . $aac;

            #pegando tempo total da musica
            $time = exec("ffmpeg -i " . escapeshellarg($fileTmp) . " 2>&1 | grep 'Duration' | cut -d ' ' -f 4"); #| sed s/,#
            list($hms, $milli) = explode('.', $time);
            list($hours, $minutes, $seconds) = explode(':', $hms);
            $totalTime = $minutes . ':' . $seconds;

            if (file_exists($fileTmp)) {

                Aws::AwsSave($path_s3, static::$path);
                exec('rm ' . static::$path . $aac);

                if (!file_exists($fileTmp)) {
                    #salva na aws via driver laravel
                    # Storage::disk('s3')->put($filePathMp3, file_get_contents($file), 'public');
                    Music::create([
                        'title' => $title,
                        'artist' => $artist,
                        'totalTime' => $totalTime,
                        'url_aac_s3' => Storage::disk('s3')->url($filePathAac),
                        'name_aac' =>  $aac,
                        'genre_id' => $genresId
                        # 'url_mp3_s3' => Storage::disk('s3')->url($filePathMp3),
                        # 'name_mp3' => $mp3,
                    ]);
                } else {
                    abort(redirect('music/create')->with('error', 'Failed to save file to the cloud'));
                }
            } else {
                abort(redirect('music/create')->with('error', 'Error compressing file'));
            }
        } else {
            abort(redirect('music/create')->with('error', 'Genders are different'));
        }
    }

    public static function saveContent($file, $category, $subCategory)
    {
        $filename = date('d.m.y') . '-' . date('H:i:s') . '.aac';

        #dir para slavar na s3 pelo cli
        $category = CategoryContent::find($category);
        $path_cli = '/content' . '/' . $category->name . '/' . $subCategory;
        $path_s3 = 'content' . '/' . $category->name . '/' . $subCategory . '/' . $filename;

        #comprimi em aac
        exec('ffmpeg -i ' . $file .  ' -c:a aac -b:a 64k ' . static::$path . $filename . ' 2>&1 1> /dev/null');

        $fileTmp = static::$path . $filename;

        #pegando tempo total da musica
        $time = exec("ffmpeg -i " . escapeshellarg($fileTmp) . " 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,#");
        list($hms, $milli) = explode('.', $time);
        list($hours, $minutes, $seconds) = explode(':', $hms);
        $totalTime = $minutes . ':' . $seconds;

        if (file_exists($fileTmp)) {
            Aws::AwsSave($path_cli, static::$path);
            exec('rm ' . static::$path . $filename);
            if (!file_exists($fileTmp)) {
                return [$filename, $path_s3, $totalTime];
            }
        } else {
            abort(redirect('content/create')->with('error', 'Error compressing file'));
        }
    }

    public static function contentClient($name, $file)
    {
        $company = Company::where('user_id', Auth::id())->get();
        $segment = Term::where('company_id', $company[0]->id)->with('segment')->get();
        $id = $company[0]->id;
        $company = $company[0]->fantasy_name;
        $segment = $segment[0]->segment->name;

        $filename = date('d.m.y') . '-' . date('H:i:s') . '-' . $company . '-' . $name . '.aac';

        $path_cli = '/content/cliente' . '/' . $company . '/' . $segment;
        $path_s3 = '/content/cliente' . '/' . $company . '/' . $segment . '/' . $filename;

        #comprimi em aac
        exec('ffmpeg -i ' . $file .  ' -c:a aac -b:a 64k ' . static::$path . $filename . ' 2>&1 1> /dev/null');

        $fileTmp = static::$path . $filename;

        #pegando tempo total da musica
        $time = exec("ffmpeg -i " . escapeshellarg($fileTmp) . " 2>&1 | grep 'Duration' | cut -d ' ' -f 4");

        list($hms, $milli) = explode('.', $time);
        list($hours, $minutes, $seconds) = explode(':', $hms);
        $totalTime = $minutes . ':' . $seconds;

        if (file_exists($fileTmp)) {
            Aws::AwsSave($path_cli, static::$path);
            exec('rm ' . static::$path . $filename);
            if (!file_exists($fileTmp)) {
                return [$filename, $path_s3, $id, $totalTime];
            }
        } else {
            return response()->json([
                'message' => 'Error saving file'
            ], 500);
        }
    }
}
