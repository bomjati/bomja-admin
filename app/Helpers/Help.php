<?php

namespace App\Helpers;

use App\Models\Media\ItemPlaylist;
use App\Models\Media\Playlist;
use Illuminate\Support\Facades\Auth;

class Help
{
    public static function removeFromList($playlists, $musics)
    {
        $dataP = array();
        $dataM = array();

        foreach ($playlists as $playlist) {
            $dataP[] = $playlist->music->id;
        }
        foreach ($musics as $music) {
            $dataM[] = $music->id;
        }

        $diff = array_intersect($dataM, $dataP);

        foreach ($diff as $key => $value) {
            $musics = $musics->except($value);
        }
        return $musics;
    }

}
