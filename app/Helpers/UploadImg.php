<?php

namespace App\Helpers;

use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\ProfileConfiguration;
use Illuminate\Support\Facades\Storage;

class UploadImg
{
    //diretorio temporario
    static protected $dir = '/var/www/html/assets/';

    public static function convertToSave($file, $img, $name_img)
    {
        list($width, $height, $type) = getimagesize($file);

        $largura = (1000 * 3) / 4;
        $altura = 1000;


        $imagem_tmp = imagecreatetruecolor($largura, $altura);
        imagesavealpha($imagem_tmp, true);
        imagealphablending($imagem_tmp, false);
        $cor_fundo = imagecolorallocatealpha($imagem_tmp, 0, 0, 0, 127);

        imagefill($img, 0, 0, $cor_fundo);
        imagecolortransparent($img, $cor_fundo);
        imagecopyresampled($imagem_tmp, $img, 0, 0, 0, 0, $largura, $altura, $width, $height);

        $t = imagepng($imagem_tmp, static::$dir . '/' . $name_img);
        imagedestroy($imagem_tmp);
    }

    public static function bottomlessPNG($file, $name_img, $path_s3)
    {
        $img_png = imagecreatefrompng($file);

        UploadImg::convertToSave($file, $img_png, $name_img);

        Aws::AwsSave($path_s3, static::$dir);
        exec('rm ' . static::$dir . $name_img);
    }

    public static function jpegToPng($file, $name_img, $path_s3)
    {
        $img_jpe = imagecreatefromjpeg($file,);

        UploadImg::convertToSave($file, $img_jpe, $name_img);

        Aws::AwsSave($path_s3, static::$dir);
        exec('rm ' . static::$dir . $name_img);
    }

    public static function deleteImg($id)
    {
        $products = Product::findOrFail($id);

        $prod_category = ProductCategory::find($products->product_category_id);

        $category = preg_replace('/[ -]+/', '-',  $prod_category->name);

        $products_s3 = Storage::disk('s3')->exists('images/produtos/' . $category . '/' . $products->productImg->nome_img);

        if ($products_s3) {

            Storage::disk('s3')->delete('images/produtos/' . $category . '/' . $products->productImg->nome_img);
        } else {
            return redirect()->route('product.index')->with('success', 'Error: Product not exists');
        }
    }

    public static function deletePicture($id)
    {
        $confi = ProfileConfiguration::find($id);

        $exist = Storage::disk('s3')->exists('images/photoProfile/', $confi->imgName);

        if ($exist) {
            Storage::disk('s3')->delete('images/photoProfile/' . $confi->imgName);
        }

        return response()->json([
            'message' => 'The image does not exist'
        ]);
    }
}
