<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');

Route::group(['middleware' => ['apiJWT']], function () {
    //controle Auth
    Route::post('logout', 'Api\AuthController@logout');
    Route::post('refresh', 'Api\AuthController@refresh');
    Route::post('me', 'Api\AuthController@me');

    //controle usuarios
    Route::get('users', 'Api\UserController@index');
    Route::get('user/show/{id}', 'Api\UserController@show');
    Route::post('users', 'Api\UserController@store');
    Route::put('users/{id}', 'Api\UserController@update');
    Route::delete('users/{id}', 'Api\UserController@destroy');
    //controle de contas
    Route::get('disable', 'Api\DisableUserController@index');
    Route::get('restore/{id}', 'Api\DisableUserController@restore');

    Route::get('config/{id}', 'Api\DisableUserController@configIndex');
    Route::post('config', 'Api\DisableUserController@configProfile');
    Route::put('config/{id}', 'Api\DisableUserController@configUpdate');
    //controle empresa
    Route::apiResource('business', 'Api\Company\CompanyController');

    Route::get('product/{id}', 'Api\Product\ProductController@index');
    Route::post('product', 'Api\Product\ProductController@store');
    Route::put('product/{id}', 'Api\Product\ProductController@update');
    Route::delete('product/{id}', 'Api\Product\ProductController@destroy');
    Route::get('product/info/{id}', 'Api\Product\ProductController@info');

    Route::get('segment', 'Api\Company\CompanyController@segment');

    Route::get('category', 'Api\Product\ProductController@category');

    Route::post('like/{id}', 'Api\Product\NetController@like');
    Route::post('view/{id}', 'Api\Product\NetController@view');

    Route::post('comment/{id}', 'Api\Product\NetController@create');
    Route::put('comment/{id}', 'Api\Product\NetController@edit');
    Route::delete('comment/{id}', 'Api\Product\NetController@destroy');

    Route::get('music', 'Api\Media\MusicController@index');

    Route::get('playlist/index', 'Api\Media\MusicController@playlistIndex');
    Route::post('playlist', 'Api\Media\MusicController@playlistStore');
    Route::put('playlist/{id}', 'Api\Media\MusicController@playlistUpdate');
    Route::delete('playlist/{id}', 'Api\Media\MusicController@playlistDestroy');
    Route::delete('playlist/item/{id}', 'Api\Media\MusicController@itemDestroy');

    Route::get('content/serve','Api\Media\ContentController@indexServe');
    Route::get('content/client','Api\Media\ContentController@indexClient');
    Route::post('content/employee','Api\Media\ContentController@storeEmployee');
    Route::post('content/client','Api\Media\ContentController@storeContent');
    Route::delete('content/{id}','Api\Media\ContentController@destroy');
});
