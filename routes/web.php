<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');

    Route::resource('company', 'Company\CompanyController');

    Route::get('offline/index', 'UserOffController@index')->name('offline.index');
    Route::get('offline/restore/{id}', 'UserOffController@restore')->name('offline.restore');

    //Media
    Route::get('music', 'Media\MusicController@index')->name('music.index');
    Route::get('music/create', 'Media\MusicController@create')->name('music.create');
    Route::get('music/{id}', 'Media\MusicController@show')->name('music.show');
    Route::post('music', 'Media\MusicController@store')->name('music.store');
    Route::delete('music/{id}', 'Media\MusicController@delete')->name('music.delete');

    Route::get('playlist/index/{id}', 'Media\MusicController@playlistIndex')->name('playlist.index');
    Route::get('playlist/create', 'Media\MusicController@playlistCreate')->name('playlist.create');
    Route::get('playlist/{id}/edit', 'Media\MusicController@playlistEdit')->name('playlist.edit');
    Route::post('playlist', 'Media\MusicController@playlistStore')->name('playlist.store');
    Route::put('playlist/{id}', 'Media\MusicController@playlistUpdate')->name('playlist.update');
    Route::get('playlist/show/{id}', 'Media\MusicController@playlistShow')->name('playlist.show');
    Route::delete('playlist/destroy/{id}', 'Media\MusicController@playlistDestroy')->name('playlist.destroy');
    Route::delete('playlist/item/{id}', 'Media\MusicController@itemDestroy')->name('playlist.delete');
    //Arcevo gênero
    Route::get('heap', 'Media\HeapController@index')->name('heap.index');
    Route::get('heap/create', 'Media\HeapController@create')->name('heap.create');
    Route::post('heap', 'Media\HeapController@store')->name('heap.store');
    Route::get('heap/{id}', 'Media\HeapController@show')->name('heap.show');

    //product
    Route::get('product', 'Product\ProductController@index')->name('product.index');
    Route::get('product/create', 'Product\ProductController@create')->name('product.create');
    Route::post('product', 'Product\ProductController@store')->name('product.store');
    Route::get('product/{id}', 'Product\ProductController@show')->name('product.show');
    Route::get('product/{id}/edit', 'Product\ProductController@edit')->name('product.edit');
    Route::put('product/{id}', 'Product\ProductController@update')->name('product.update');
    Route::delete('product/{id}', 'Product\ProductController@destroy')->name('product.destroy');

    Route::post('product/like/{id}', 'Product\ProductController@like')->name('product.like');
    Route::post('product/view/{id}', 'Product\ProductController@view')->name('product.view');

    Route::post('comment/create/{id}', 'Product\ProductController@comment')->name('comment.create');
    Route::get('comment/{id}/edit', 'Product\ProductController@commentEdit')->name('comment.edit');
    Route::put('comment/{id}', 'Product\ProductController@commentUpdate')->name('comment.update');
    Route::delete('comment/{id}', 'Product\ProductController@commentDestroy')->name('comment.destroy');

    Route::get('content', 'Content\ContentController@index')->name('content.index');
    Route::get('content/create', 'Content\ContentController@create')->name('content.create');
    Route::post('content', 'Content\ContentController@store')->name('content.store');
    Route::delete('content/{id}', 'Content\ContentController@destroy')->name('content.destroy');
});
